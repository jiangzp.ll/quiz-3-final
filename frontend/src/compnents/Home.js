import React, {Component} from 'react';
import {connect} from 'react-redux';

function mapStateToProps(state) {
  return {};
}

class Home extends Component {


  constructor(props, context) {
    super(props, context);
    this.state = {
      value: ""
    }
    this.helloWorld = this.helloWorld.bind(this);
  }

  render() {
    return (
      <div>

        <button onClick={this.helloWorld}>hello</button>
      </div>
    );
  }

  helloWorld() {
    fetch("http://localhost:8080/api").then(response => {

      response.text().then(result => {

      })
    })
  }
}

export default connect(
  mapStateToProps,
)(Home);
