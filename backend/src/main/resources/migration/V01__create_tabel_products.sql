CREATE TABLE IF NOT EXISTS products (
    id BIGINT AUTO_INCREMENT,
    name varchar(255) not null,
    price double not null,
    commodity_unit varchar(255) not null,
    image_link varchar(255) not null,
    primary key (id)
);
