package com.twuc.webapp.backend.contract;

import javax.validation.constraints.NotNull;

public class CreateProductRequest {

    @NotNull
    private String name;

    @NotNull
    private Double price;

    @NotNull
    private String commodityUnit;

    @NotNull
    private String imageLink;

    public CreateProductRequest() {
    }

    public CreateProductRequest(@NotNull String name, @NotNull Double price, @NotNull String commodityUnit, @NotNull String imageLink) {
        this.name = name;
        this.price = price;
        this.commodityUnit = commodityUnit;
        this.imageLink = imageLink;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getCommodityUnit() {
        return commodityUnit;
    }

    public String getImageLink() {
        return imageLink;
    }
}
