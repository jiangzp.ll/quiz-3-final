package com.twuc.webapp.backend.controller;

import com.twuc.webapp.backend.contract.CreateProductRequest;
import com.twuc.webapp.backend.contract.GetProductResponse;
import com.twuc.webapp.backend.entity.Product;
import com.twuc.webapp.backend.repository.ProductRepository;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/products")
@CrossOrigin(origins = "*")
public class ProductController {

    private ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping
    public ResponseEntity<List<GetProductResponse>> getAll() {
        List<GetProductResponse> products = productRepository.findAll(
                Sort.by(Sort.Direction.ASC, "id")).stream()
                .map(p -> new GetProductResponse(p.getId(), p.getName(), p.getPrice(), p.getCommodityUnit(), p.getImageLink()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(products);
    }

    @PostMapping
    public ResponseEntity create(@RequestBody @Valid CreateProductRequest request) {
        if (request == null) return ResponseEntity.badRequest().build();
        productRepository.save(new Product(request.getName(), request.getPrice(), request.getCommodityUnit(), request.getImageLink()));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity delete(@PathVariable Long productId) {
        Optional<Product> post = productRepository.findById(productId);
        post.ifPresent(productRepository::delete);
        return ResponseEntity.ok().build();
    }
}
