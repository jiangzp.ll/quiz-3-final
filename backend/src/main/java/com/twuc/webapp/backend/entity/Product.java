package com.twuc.webapp.backend.entity;

import javax.persistence.*;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private String commodityUnit;

    @Column(nullable = false)
    private String imageLink;

    public Product() {
    }

    public Product(String name, Double price, String commodityUnit, String imageLink) {
        this.name = name;
        this.price = price;
        this.commodityUnit = commodityUnit;
        this.imageLink = imageLink;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getCommodityUnit() {
        return commodityUnit;
    }

    public String getImageLink() {
        return imageLink;
    }
}
