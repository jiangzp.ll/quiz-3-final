package com.twuc.webapp.backend.contract;

public class GetProductResponse {
    private Long id;
    private String name;
    private Double price;
    private String commodityUnit;
    private String imageLink;

    public GetProductResponse() {
    }

    public GetProductResponse(Long id, String name, Double price, String commodityUnit, String imageLink) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.commodityUnit = commodityUnit;
        this.imageLink = imageLink;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getCommodityUnit() {
        return commodityUnit;
    }

    public String getImageLink() {
        return imageLink;
    }
}
