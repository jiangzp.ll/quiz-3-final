package com.twuc.webapp.backend.repository;

import com.twuc.webapp.backend.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
