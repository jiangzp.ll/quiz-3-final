package com.twuc.webapp.backend.repository;

import com.twuc.webapp.backend.entity.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void should_save_product_to_db() {
        Product product = new Product("Cola", 3.0, "drink", "a");
        productRepository.save(product);
        entityManager.flush();
        entityManager.clear();
        Product fetchProduct = productRepository.findById(product.getId()).get();
        assertNotNull(fetchProduct);
        assertEquals("Cola", fetchProduct.getName());
        assertEquals(Double.valueOf(3.0), fetchProduct.getPrice());
        assertEquals("drink", fetchProduct.getCommodityUnit());
        assertEquals("a", fetchProduct.getImageLink());
    }
}
