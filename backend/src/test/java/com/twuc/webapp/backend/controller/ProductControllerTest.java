package com.twuc.webapp.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webapp.backend.ApiTestBase;
import com.twuc.webapp.backend.contract.GetProductResponse;
import com.twuc.webapp.backend.entity.Product;
import com.twuc.webapp.backend.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ProductControllerTest extends ApiTestBase {

    @Autowired
    private ProductRepository productRepository;

    private List<Long> createProduct(List<Product> products) {
        List<Long> ids = new ArrayList<>();

        clear(em -> {
            productRepository.saveAll(products);
            productRepository.flush();
            ids.addAll(products.stream().map(Product::getId).collect(Collectors.toList()));
        });

        return ids;
    }

    @Test
    void should_get_all_products() throws Exception {
        createProduct(Arrays.asList(new Product("Cola", 3.0, "drink", "a"),
                new Product("laTiao", 5.0, "junkFood", "b")));

        String json = getMockMvc().perform(get("/api/products"))
                .andExpect(status().is(200))
                .andReturn().getResponse().getContentAsString();

        GetProductResponse[] products = new ObjectMapper().readValue(json, GetProductResponse[].class);

        assertEquals(2, products.length);
        assertEquals("Cola", products[0].getName());
        assertEquals(Double.valueOf(3.0), products[0].getPrice());
        assertEquals("drink", products[0].getCommodityUnit());
        assertEquals("a", products[0].getImageLink());
        assertEquals("laTiao", products[1].getName());
        assertEquals(Double.valueOf(5.0), products[1].getPrice());
        assertEquals("junkFood", products[1].getCommodityUnit());
        assertEquals("b", products[1].getImageLink());

    }

    @Test
    void should_get_zero_products() throws Exception {
        String json = getMockMvc().perform(get("/api/products"))
                .andExpect(status().is(200))
                .andReturn().getResponse().getContentAsString();

        ProductResponseForTest[] posts = new ObjectMapper().readValue(json, ProductResponseForTest[].class);

        assertEquals(0, posts.length);
    }

    @Test
    void should_create_products() throws Exception {
        getMockMvc().perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(
                        new CreateProductRequestForTest("Cola", 3.0, "drink", "a"))))
                .andExpect(status().is(201));

        List<Product> products = productRepository.findAll();

        assertEquals(1, products.size());
        assertEquals("Cola", products.get(0).getName());
        assertEquals(Double.valueOf(3.0), products.get(0).getPrice());
        assertEquals("drink", products.get(0).getCommodityUnit());
        assertEquals("a", products.get(0).getImageLink());
    }

    @Test
    void should_remove_products() throws Exception {
        List<Long> ids = createProduct(
                Arrays.asList(
                        new Product("Cola", 3.0, "drink", "a"),
                        new Product("laTiao", 5.0, "junkFood", "b")));

        getMockMvc().perform(delete("/api/products/" + ids.get(1)))
                .andExpect(status().is(200));

        List<Product> posts = productRepository.findAll();
        assertEquals(1, posts.size());
        assertEquals(ids.get(0), posts.get(0).getId());
    }

    static class ProductResponseForTest {

        private String name;
        private Double price;
        private String commodityUnit;
        private String imageLink;

        ProductResponseForTest() {
        }

        public String getName() {
            return name;
        }

        public Double getPrice() {
            return price;
        }

        public String getCommodityUnit() {
            return commodityUnit;
        }

        public String getImageLink() {
            return imageLink;
        }
    }

    static class CreateProductRequestForTest {
        private String name;
        private Double price;
        private String commodityUnit;
        private String imageLink;

        CreateProductRequestForTest(String name, Double price, String commodityUnit, String imageLink) {
            this.name = name;
            this.price = price;
            this.commodityUnit = commodityUnit;
            this.imageLink = imageLink;
        }

        public String getName() {
            return name;
        }

        public Double getPrice() {
            return price;
        }

        public String getCommodityUnit() {
            return commodityUnit;
        }

        public String getImageLink() {
            return imageLink;
        }
    }
}
